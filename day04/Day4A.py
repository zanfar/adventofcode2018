#!/usr/bin/env python3

from math import floor, log
from enum import Enum
import re
import json

INPUT_FILENAME = 'Day4Input.txt'
SAMPLE_LINES = 10
SAMPLE_LINE_DIGITS = floor(log(SAMPLE_LINES) / log(10)) + 1
fmt_str = '{:' + str(SAMPLE_LINE_DIGITS) + 'd} {:s}'

class ReadState(Enum):
	IDLE = 0
	AWAKE = 1
	ASLEEP = 2

def read_input(infile_path = INPUT_FILENAME):
	with open(infile_path, 'r') as infile:
		for line in infile:
			yield line.strip()

print()
print('First 10 lines of input:')
for linenum,line in enumerate(read_input()):
	if linenum >= SAMPLE_LINES:
		break
	print(fmt_str.format(linenum+1, line))

def sort_input(line_iter):
	for line in sorted(line_iter):
		yield line

print()
print('First 10 lines of sorted input:')
for linenum,line in enumerate(sort_input(read_input())):
	if linenum >= SAMPLE_LINES:
		break
	print(fmt_str.format(linenum+1, line))

print()
current_state = ReadState.IDLE
current_guard = None
nap_start = None

# [1518-04-01 00:00] Guard #3167 begins shift
# [1518-04-01 00:53] falls asleep
# [1518-04-01 00:54] wakes up
RE_RECORD = re.compile('\[[0-9]{4}-(?P<date>[0-9]{2}-[0-9]{2})\s+(?P<time>[0-9]{2}:[0-9]{2})\]\s+(?P<event>Guard #(?P<gid>[0-9]+) begins shift|falls asleep|wakes up)', flags=re.IGNORECASE)

history = dict()

for record in sort_input(read_input()):
	match = RE_RECORD.match(record)

	if not match:
		raise ValueError('Invalid record format: {:s}'.format(record))

	event = match.group('event')
	stat = event[0:5].lower()

	date = match.group('date')
	month = int(date[0:2])
	day = int(date[3:5])

	time = match.group('time')
	hour = int(time[0:2])
	minute = int(time[3:5])

	if current_state is ReadState.IDLE:
	
		if stat == 'guard':
			current_guard = int(match.group('gid'))
			current_state = ReadState.AWAKE

			if current_guard not in history:
				history[current_guard] = {
					'shifts': {},
					'minutes': {},
					}

			continue

		else:
			raise ValueError("Invalid record '{:s}' in state '{:s}'".format(record,current_state))

	elif current_state is ReadState.AWAKE:
	
		if stat == 'guard':
			current_guard = int(match.group('gid'))
			current_state = ReadState.AWAKE

			if current_guard not in history:
				history[current_guard] = {
					'shifts': {},
					'minutes': {},
					}

			continue

		elif stat == 'falls':
			nap_start = minute
			current_state = ReadState.ASLEEP

			continue

		else:
			raise ValueError("Invalid record '{:s}' in state '{:s}'".format(record,current_state))

	elif current_state is ReadState.ASLEEP:

		if stat == 'wakes':
			if minute > nap_start:
				minutes = list(range(nap_start, minute))
			else:
				minutes = list(range(nap_start, 0)) + list(range(0, minute))

			history[current_guard]['shifts'][date] = {
					'start': nap_start,
					'end': minute,
					}

			for m in minutes:
				if m not in history[current_guard]['minutes']:
					history[current_guard]['minutes'][m] = 0

				history[current_guard]['minutes'][m] += 1

			current_state = ReadState.AWAKE

			print("Guard {:d} slept from {:d} to {:d} on {:s}".format(
				current_guard, nap_start, minute, date))

			continue

print()

sleepiest_guard = None
sleepiest_minutes = 0

for gid in sorted(history.keys()):
	minutes_asleep = sum(history[gid]['minutes'].values())
	if minutes_asleep > sleepiest_minutes:
		sleepiest_guard = gid
		sleepiest_minutes = minutes_asleep

	print("Guard {:d} slept for {:d} total minutes".format(gid, minutes_asleep))

print()
sleepiest_days = 0
sleepiest_minute = None
for m, days in history[sleepiest_guard]['minutes'].items():
	if days > sleepiest_days:
		sleepiest_minute = m
		sleepiest_days = days
print("Guard {:d} sleeps most at minute {:d}".format(sleepiest_guard, sleepiest_minute))

print()
print("Day 4, part A answer: {:d}".format(
	sleepiest_guard * sleepiest_minute))

most_slept_guard = None
most_slept_days = 0
most_slept_minute = 0

print()
for gid in history:
	for m, days in history[gid]['minutes'].items():
		if days > most_slept_days:
			most_slept_guard = gid
			most_slept_days = days
			most_slept_minute = m

print("Most slept minute is {:d} by guard {:d} for {:d} days".format(most_slept_minute, most_slept_guard, most_slept_days))
print()
print("Dy 4, Part B answer: {:d}".format(most_slept_guard * most_slept_minute))

